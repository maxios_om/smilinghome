//
//  DeviceDetailsViewController.h
//  SmilingHome
//
//  Created by Om's on 10/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SMA_Devices;
@protocol DeviceDetailsViewControllerDelegate ;

@interface DeviceDetailsViewController : UITableViewController

@property (nonatomic , unsafe_unretained) id <DeviceDetailsViewControllerDelegate>delegate;
@property (nonatomic , unsafe_unretained) BOOL addNew;
@property (nonatomic , strong) SMA_Devices *selectedDevice;

@end

@protocol DeviceDetailsViewControllerDelegate <NSObject>

@required

- (void)deviceDetailsViewControllerDidSave:(DeviceDetailsViewController *)controller;

-(void)deviceDetailsViewControllerDidCancel:(DeviceDetailsViewController *)controller;

@end
