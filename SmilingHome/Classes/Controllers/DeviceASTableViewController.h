//
//  DeviceASTableViewController.h
//  SmilingHome
//
//  Created by Om's on 12/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SMA_Devices;
@interface DeviceASTableViewController : UITableViewController

@property (nonatomic , strong) SMA_Devices *selectedDevice;
@end
