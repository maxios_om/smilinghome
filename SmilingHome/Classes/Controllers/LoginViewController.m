//
//  LoginViewController.m
//  SmilingHome
//
//  Created by Om's on 12/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import "LoginViewController.h"
#import "SMA_Users.h"
#import "UserSessionManager.h"
#import "UserDevicesTableViewController.h"
#import "NSString+OK_Additions.h"

@interface LoginViewController ()
@property (strong, nonatomic) IBOutlet UITextField *tfEmail;
@property (strong, nonatomic) IBOutlet UITextField *tfPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;
@property (strong, nonatomic) IBOutlet UIButton *btnFPass;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	[self.tfEmail setText:@"khedekar.omkar@gmail.com"];
	[self.tfPassword setText:@"#MyP@ri2016"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)loginButtonTapped:(id)sender {

	NSString *userNameStr = [self.tfEmail text];
	NSString *userPassStr = [self.tfPassword text];

	if (![userNameStr ok_IsBlank] && ![userPassStr ok_IsBlank]) {
		[SMA_Users LoginUserWith:userNameStr password:userPassStr andHandler:^(id objectsOrObject, NSError *error) {
			if (!error) {
				NSDictionary *userJSON = [(NSDictionary *) objectsOrObject objectForKey:@"user"];
				SMA_Users *currentUser = [SMA_Users modelObjectWithDictionary:userJSON];
				[[UserSessionManager sessionManager] setCurrentUser:currentUser];
				[self performSegueWithIdentifier:@"ShowDevies" sender:self];
			}
		}];
	}

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//ShowDevies
/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	UINavigationController *navigationController = segue.destinationViewController;
	UserDevicesTableViewController *devicesViewController = [[navigationController viewControllers] objectAtIndex:0];
	
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
