//
//  DeviceASTableViewController.m
//  SmilingHome
//
//  Created by Om's on 12/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import "DeviceASTableViewController.h"
#import "SMA_Devices.h"
#import "SMA_Actuators.h"
#import "SMA_Sensors.h"

static NSString *DeviceASTableCell = @"DeviceASTableCell";

@interface DeviceASTableViewController ()

@end

@implementation DeviceASTableViewController

- (void)viewDidLoad {
	[super viewDidLoad];

	// Uncomment the following line to preserve selection between presentations.
	// self.clearsSelectionOnViewWillAppear = NO;

	// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
	// self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	//#warning Potentially incomplete method implementation.
	// Return the number of sections.
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	//#warning Incomplete method implementation.
	// Return the number of rows in the section.
	int noOfRows = 0;
	switch (section) {
		case 0: //Actuator
		{
			noOfRows = [self.selectedDevice.actuators count];
		}
			break;
		case 1: //Sensor
		{
			noOfRows = [self.selectedDevice.sensors count];
		}
			break;

		default:
			break;
	}
	return noOfRows;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *sectionTitle = @"";
	switch (section) {
		case 0: //Actuator
		{
			sectionTitle = @"Actuators";
		}
			break;
		case 1: //Sensor
		{
			sectionTitle = @"Sensors";
		}
			break;

		default:
			break;
	}
	return sectionTitle;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DeviceASTableCell];

	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:DeviceASTableCell];
	}

	switch (indexPath.section) {
		case 0: //Actuator
		{
			SMA_Actuators *act = self.selectedDevice.actuators[indexPath.row];
			[cell.textLabel setText: act.actuatorName];
		}
			break;
		case 1: //Sensor
		{
			SMA_Sensors *sens = self.selectedDevice.sensors[indexPath.row];
			[cell.textLabel setText: sens.sensorName];
		}

			break;

		default:
			break;
	}

	return cell;
}

/*
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DeviceASTableCell];

	if (!cell) {
 cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:DeviceASTableCell];
	}

	switch (section) {
 case 1: //Actuator
 {
 SMA_Actuators *act = self.selectedDevice.actuators[];
 }
 break;
 case 2: //Sensor

 break;

 default:
 break;
	}

 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
