//
//  UserSessionManager.h
//  SmilingHome
//
//  Created by Om's on 10/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SMA_Users;
@interface UserSessionManager : NSObject

@property (nonatomic , strong) SMA_Users *currentUser;

+(instancetype)sessionManager;

@end
