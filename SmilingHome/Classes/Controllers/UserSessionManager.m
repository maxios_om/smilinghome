//
//  UserSessionManager.m
//  SmilingHome
//
//  Created by Om's on 10/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import "UserSessionManager.h"
#import "SMA_Users.h"



@implementation UserSessionManager

+(instancetype)sessionManager {
	static UserSessionManager *manager  = nil;

	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		manager = [[UserSessionManager alloc] init];
	});

	return manager;
}




@end
