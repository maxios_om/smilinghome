//
//  DeviceDetailsViewController.m
//  SmilingHome
//
//  Created by Om's on 10/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import "DeviceDetailsViewController.h"
#import "SMA_Devices.h"
#import "NSString+OK_Additions.h"
#import "UserSessionManager.h"
#import "SMA_Users.h"
#import <MBProgressHUD.h>



@interface DeviceDetailsViewController ()

@property (strong, nonatomic) IBOutlet UITextField *tfDeviceName;
@property (strong, nonatomic) IBOutlet UISwitch *isActive;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;

@end

@implementation DeviceDetailsViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		_addNew = NO;
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];

	if(!_delegate){
		self.navigationItem.leftBarButtonItem = nil;

	}

	if (_selectedDevice) {
		[self.tfDeviceName setText:self.selectedDevice.deviceName];
		[self.isActive setOn:self.selectedDevice.isActive animated:YES];
	} else {
		self.navigationItem.rightBarButtonItem = nil;
	}


	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


#pragma mark -  actions

- (IBAction)cancel:(id)sender {
	[self.delegate deviceDetailsViewControllerDidCancel:self];
}
- (IBAction)done:(id)sender {

	NSString *deviceName = [self.tfDeviceName text];

	if ([deviceName ok_IsBlank]) {
		UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Hagarya" message:@"nav tak adhi" preferredStyle:UIAlertControllerStyleAlert];
		[alert addAction: [UIAlertAction actionWithTitle:@"OKKKKKKK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
			DebugLog(@"takato parat");
		}]];

		[self presentViewController:alert animated:YES completion:nil];

	} else {
		MBProgressHUD *hud = [MBProgressHUD HUDForView:self.navigationController.view];
		[self.navigationController.view addSubview:hud];

		if (self.addNew) {
			[SMA_Devices AddDeviceForUser:[[[UserSessionManager sessionManager] currentUser] userId] withName:deviceName handler:^(id objectsOrObject, NSError *error) {
				[MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
				if (!error) {

					DebugLog(@"Resp : %@", objectsOrObject);
					[self.delegate deviceDetailsViewControllerDidSave:self];
				} else {

					OK_ERROR(@"%@", error);
				}

			}];
		} else {
			[self.selectedDevice setDeviceName:self.tfDeviceName.text];
			[self.selectedDevice setIsActive:self.isActive.isOn];
			[self.selectedDevice updateWithHandler:^(id objectsOrObject, NSError *error) {
				[MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
				if (!error) {

					DebugLog(@"Resp : %@", objectsOrObject);
					[self.delegate deviceDetailsViewControllerDidSave:self];
				} else {

					OK_ERROR(@"%@", error);
				}
			}];
		}
	}

}


#pragma mark - UITableViewDelegate & Datasource

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0) {

		[self.tfDeviceName becomeFirstResponder];

	}
}

/*
 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
