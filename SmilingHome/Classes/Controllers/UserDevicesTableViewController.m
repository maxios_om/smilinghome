//
//  UserDevicesTableViewController.m
//  SmilingHome
//
//  Created by Om's on 09/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import "UserDevicesTableViewController.h"
#import "SMA_Users.h"
#import "SMA_Devices.h"
#import "UserSessionManager.h"
#import "DeviceDetailsViewController.h"
#import "DeviceASTableViewController.h"


static NSString * deviceCellID = @"SMHDeviceCell";

@interface UserDevicesTableViewController () <DeviceDetailsViewControllerDelegate>

@property (nonatomic , strong) NSMutableArray *devices;

@end

@implementation UserDevicesTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];

	SMA_Users *selectedUser = [[UserSessionManager sessionManager] currentUser];
	[self setTitle:selectedUser.nickName];
//	[self refresh];

    // Uncomment the following line to preserve selection between presentations.
//     self.clearsSelectionOnViewWillAppear = NO;

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
		[self refresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [self.devices count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:deviceCellID];

	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:deviceCellID];
	}

	SMA_Devices *device = self.devices[indexPath.row];
	[cell.textLabel setText:device.deviceName];
	[cell.detailTextLabel setText:[NSString stringWithFormat:@"Updated on : %@", device.updatedOn]];
	[cell setAccessoryType:UITableViewCellAccessoryDetailButton];
    return cell;
}
/**/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Internal
- (void)refresh {

	SMA_Users *selectedUser = [[UserSessionManager sessionManager] currentUser];
	[SMA_Devices GetDevicesForUser:selectedUser.userId withHandler:^(NSMutableArray* objectsOrObject, NSError *error) {

		if (objectsOrObject) {
			self.devices = objectsOrObject;
			[self.tableView reloadData];
		}
	}];
}



#pragma mark - UIStoryboardSegue


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"DeviceDetails"])
	{
		NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
		SMA_Devices *selectedDevice = self.devices[selectedIndexPath.row];
		UINavigationController *navigationController = segue.destinationViewController;
		DeviceDetailsViewController *deviceDetailsViewController = [[navigationController viewControllers] objectAtIndex:0];
		[deviceDetailsViewController setTitle:selectedDevice.deviceName];
		[deviceDetailsViewController setSelectedDevice:selectedDevice];

	}

	if ([[segue identifier] isEqualToString:@"AddDevice"])
	{

		UINavigationController *navigationController = segue.destinationViewController;

		DeviceDetailsViewController *deviceDetailsViewController = [[navigationController viewControllers] objectAtIndex:0];
		deviceDetailsViewController.delegate = self;
		[deviceDetailsViewController setTitle:@"Add New "];
		[deviceDetailsViewController setAddNew:YES];
	}

//	ShowASD

	if ([[segue identifier] isEqualToString:@"ShowASD"])
	{

		NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
		SMA_Devices *selectedDevice = self.devices[selectedIndexPath.row];
		UINavigationController *navigationController = segue.destinationViewController;
		DeviceASTableViewController *deviceDetailsViewController = [[navigationController viewControllers] objectAtIndex:0];
		[deviceDetailsViewController setTitle:selectedDevice.deviceName];
		[deviceDetailsViewController setSelectedDevice:selectedDevice];

	}
}

#pragma mark - DeviceDetailsViewControllerDelegate

-(void)deviceDetailsViewControllerDidSave:(DeviceDetailsViewController *)controller {

	[controller dismissViewControllerAnimated:YES completion:nil];


}

-(void)deviceDetailsViewControllerDidCancel:(DeviceDetailsViewController *)controller {
	[controller dismissViewControllerAnimated:YES completion:nil];
}

@end
