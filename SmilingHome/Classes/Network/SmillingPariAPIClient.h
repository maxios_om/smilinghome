//
//  SmillingPariAPIClient.h
//  SmilingHome
//
//  Created by Om's on 09/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface SmillingPariAPIClient : AFHTTPSessionManager


+ (instancetype)sharedClient ;

@end
