//
//  SmillingPariAPIClient.m
//  SmilingHome
//
//  Created by Om's on 09/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import "SmillingPariAPIClient.h"

static NSString * const AFAppDotNetAPIBaseURLString = @"http://api.smillingpari.com/";

@implementation SmillingPariAPIClient

+ (instancetype)sharedClient {
	static SmillingPariAPIClient *_sharedClient = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_sharedClient = [[SmillingPariAPIClient alloc] initWithBaseURL:[NSURL URLWithString:AFAppDotNetAPIBaseURLString]];
//		_sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
	});
	return _sharedClient;
}
@end
