//
//  SMA_Details.m
//
//  Created by Omkar Khedekar on 12/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SMA_Details.h"
#import "SMA_Sensors.h"
#import "SMA_Actuators.h"


NSString *const kSMA_DetailsSensors = @"sensors";
NSString *const kSMA_DetailsActuators = @"actuators";
NSString *const kSMA_DetailsDeviceId = @"device_id";
NSString *const kSMA_DetailsIsActive = @"is_active";
NSString *const kSMA_DetailsDeviceName = @"device_name";


@interface SMA_Details ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SMA_Details

@synthesize sensors = _sensors;
@synthesize actuators = _actuators;
@synthesize deviceId = _deviceId;
@synthesize isActive = _isActive;
@synthesize deviceName = _deviceName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedSMA_Sensors = [dict objectForKey:kSMA_DetailsSensors];
    NSMutableArray *parsedSMA_Sensors = [NSMutableArray array];
    if ([receivedSMA_Sensors isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedSMA_Sensors) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedSMA_Sensors addObject:[SMA_Sensors modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedSMA_Sensors isKindOfClass:[NSDictionary class]]) {
       [parsedSMA_Sensors addObject:[SMA_Sensors modelObjectWithDictionary:(NSDictionary *)receivedSMA_Sensors]];
    }

    self.sensors = [NSArray arrayWithArray:parsedSMA_Sensors];
    NSObject *receivedSMA_Actuators = [dict objectForKey:kSMA_DetailsActuators];
    NSMutableArray *parsedSMA_Actuators = [NSMutableArray array];
    if ([receivedSMA_Actuators isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedSMA_Actuators) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedSMA_Actuators addObject:[SMA_Actuators modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedSMA_Actuators isKindOfClass:[NSDictionary class]]) {
       [parsedSMA_Actuators addObject:[SMA_Actuators modelObjectWithDictionary:(NSDictionary *)receivedSMA_Actuators]];
    }

    self.actuators = [NSArray arrayWithArray:parsedSMA_Actuators];
            self.deviceId = [[self objectOrNilForKey:kSMA_DetailsDeviceId fromDictionary:dict] doubleValue];
            self.isActive = [[self objectOrNilForKey:kSMA_DetailsIsActive fromDictionary:dict] doubleValue];
            self.deviceName = [self objectOrNilForKey:kSMA_DetailsDeviceName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForSensors = [NSMutableArray array];
    for (NSObject *subArrayObject in self.sensors) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForSensors addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForSensors addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForSensors] forKey:kSMA_DetailsSensors];
    NSMutableArray *tempArrayForActuators = [NSMutableArray array];
    for (NSObject *subArrayObject in self.actuators) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForActuators addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForActuators addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForActuators] forKey:kSMA_DetailsActuators];
    [mutableDict setValue:[NSNumber numberWithDouble:self.deviceId] forKey:kSMA_DetailsDeviceId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isActive] forKey:kSMA_DetailsIsActive];
    [mutableDict setValue:self.deviceName forKey:kSMA_DetailsDeviceName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.sensors = [aDecoder decodeObjectForKey:kSMA_DetailsSensors];
    self.actuators = [aDecoder decodeObjectForKey:kSMA_DetailsActuators];
    self.deviceId = [aDecoder decodeDoubleForKey:kSMA_DetailsDeviceId];
    self.isActive = [aDecoder decodeDoubleForKey:kSMA_DetailsIsActive];
    self.deviceName = [aDecoder decodeObjectForKey:kSMA_DetailsDeviceName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_sensors forKey:kSMA_DetailsSensors];
    [aCoder encodeObject:_actuators forKey:kSMA_DetailsActuators];
    [aCoder encodeDouble:_deviceId forKey:kSMA_DetailsDeviceId];
    [aCoder encodeDouble:_isActive forKey:kSMA_DetailsIsActive];
    [aCoder encodeObject:_deviceName forKey:kSMA_DetailsDeviceName];
}

- (id)copyWithZone:(NSZone *)zone
{
    SMA_Details *copy = [[SMA_Details alloc] init];
    
    if (copy) {

        copy.sensors = [self.sensors copyWithZone:zone];
        copy.actuators = [self.actuators copyWithZone:zone];
        copy.deviceId = self.deviceId;
        copy.isActive = self.isActive;
        copy.deviceName = [self.deviceName copyWithZone:zone];
    }
    
    return copy;
}


@end
