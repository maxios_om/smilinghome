//
//  SMA_Sensors.m
//
//  Created by Omkar Khedekar on 12/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SMA_Sensors.h"


NSString *const kSMA_SensorsSensorName = @"sensor_name";
NSString *const kSMA_SensorsSensorId = @"sensor_id";
NSString *const kSMA_SensorsCreatedOn = @"created_on";
NSString *const kSMA_SensorsIsActive = @"is_active";
NSString *const kSMA_SensorsUpdatedOn = @"updated_on";


@interface SMA_Sensors ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SMA_Sensors

@synthesize sensorName = _sensorName;
@synthesize sensorId = _sensorId;
@synthesize createdOn = _createdOn;
@synthesize isActive = _isActive;
@synthesize updatedOn = _updatedOn;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.sensorName = [self objectOrNilForKey:kSMA_SensorsSensorName fromDictionary:dict];
            self.sensorId = [[self objectOrNilForKey:kSMA_SensorsSensorId fromDictionary:dict] doubleValue];
            self.createdOn = [self objectOrNilForKey:kSMA_SensorsCreatedOn fromDictionary:dict];
            self.isActive = [[self objectOrNilForKey:kSMA_SensorsIsActive fromDictionary:dict] doubleValue];
            self.updatedOn = [self objectOrNilForKey:kSMA_SensorsUpdatedOn fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.sensorName forKey:kSMA_SensorsSensorName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sensorId] forKey:kSMA_SensorsSensorId];
    [mutableDict setValue:self.createdOn forKey:kSMA_SensorsCreatedOn];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isActive] forKey:kSMA_SensorsIsActive];
    [mutableDict setValue:self.updatedOn forKey:kSMA_SensorsUpdatedOn];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.sensorName = [aDecoder decodeObjectForKey:kSMA_SensorsSensorName];
    self.sensorId = [aDecoder decodeDoubleForKey:kSMA_SensorsSensorId];
    self.createdOn = [aDecoder decodeObjectForKey:kSMA_SensorsCreatedOn];
    self.isActive = [aDecoder decodeDoubleForKey:kSMA_SensorsIsActive];
    self.updatedOn = [aDecoder decodeObjectForKey:kSMA_SensorsUpdatedOn];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_sensorName forKey:kSMA_SensorsSensorName];
    [aCoder encodeDouble:_sensorId forKey:kSMA_SensorsSensorId];
    [aCoder encodeObject:_createdOn forKey:kSMA_SensorsCreatedOn];
    [aCoder encodeDouble:_isActive forKey:kSMA_SensorsIsActive];
    [aCoder encodeObject:_updatedOn forKey:kSMA_SensorsUpdatedOn];
}

- (id)copyWithZone:(NSZone *)zone
{
    SMA_Sensors *copy = [[SMA_Sensors alloc] init];
    
    if (copy) {

        copy.sensorName = [self.sensorName copyWithZone:zone];
        copy.sensorId = self.sensorId;
        copy.createdOn = [self.createdOn copyWithZone:zone];
        copy.isActive = self.isActive;
        copy.updatedOn = [self.updatedOn copyWithZone:zone];
    }
    
    return copy;
}


@end
