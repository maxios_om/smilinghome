//
//  SMA_Actuators.h
//
//  Created by Omkar Khedekar on 12/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "AbstractModel.h"



@interface SMA_Actuators : AbstractModel <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *updatedOn;
@property (nonatomic, assign) int actuatorId;
@property (nonatomic, assign) int deviceId;
@property (nonatomic, assign) double actualValue;
@property (nonatomic, strong) NSString *createdOn;
@property (nonatomic, assign) BOOL isActive;
@property (nonatomic, strong) NSString *actuatorName;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
