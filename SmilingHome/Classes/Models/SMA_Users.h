//
//  SMA_Users.h
//
//  Created by Omkar Khedekar on 12/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "AbstractModel.h"



@interface SMA_Users : AbstractModel <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *userEmail;
@property (nonatomic, strong) NSString *createdOn;
@property (nonatomic, assign) int userId;
@property (nonatomic, assign) BOOL isActive;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;

+(void)LoginUserWith:(NSString *)userName password:(NSString *)password andHandler:(callbackHandler) handler;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

- (NSDictionary *)dictionaryRepresentation;



@end
