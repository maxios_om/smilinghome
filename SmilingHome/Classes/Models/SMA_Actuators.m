//
//  SMA_Actuators.m
//
//  Created by Omkar Khedekar on 12/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SMA_Actuators.h"


NSString *const kSMA_ActuatorsUpdatedOn = @"updated_on";
NSString *const kSMA_ActuatorsActuatorId = @"actuator_id";
NSString *const kSMA_ActuatorsDeviceId = @"device_id";
NSString *const kSMA_ActuatorsActualValue = @"actual_value";
NSString *const kSMA_ActuatorsCreatedOn = @"created_on";
NSString *const kSMA_ActuatorsIsActive = @"is_active";
NSString *const kSMA_ActuatorsActuatorName = @"actuator_name";


@interface SMA_Actuators ()


@end

@implementation SMA_Actuators

@synthesize updatedOn = _updatedOn;
@synthesize actuatorId = _actuatorId;
@synthesize deviceId = _deviceId;
@synthesize actualValue = _actualValue;
@synthesize createdOn = _createdOn;
@synthesize isActive = _isActive;
@synthesize actuatorName = _actuatorName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super initWithDictionary:dict];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.updatedOn = [dict ok_safeStringValueForKey:kSMA_ActuatorsUpdatedOn];
            self.actuatorId = [dict ok_intForKey:kSMA_ActuatorsActuatorId];
            self.deviceId = [dict ok_intForKey:kSMA_ActuatorsDeviceId];
            self.actualValue = [dict ok_doubleForKey:kSMA_ActuatorsActualValue];
            self.createdOn = [dict ok_safeStringValueForKey:kSMA_ActuatorsCreatedOn];
            self.isActive = [dict ok_boolForKey:kSMA_ActuatorsIsActive];
            self.actuatorName = [dict ok_safeStringValueForKey:kSMA_ActuatorsActuatorName];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.updatedOn forKey:kSMA_ActuatorsUpdatedOn];
    [mutableDict setValue:[NSNumber numberWithDouble:self.actuatorId] forKey:kSMA_ActuatorsActuatorId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.deviceId] forKey:kSMA_ActuatorsDeviceId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.actualValue] forKey:kSMA_ActuatorsActualValue];
    [mutableDict setValue:self.createdOn forKey:kSMA_ActuatorsCreatedOn];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isActive] forKey:kSMA_ActuatorsIsActive];
    [mutableDict setValue:self.actuatorName forKey:kSMA_ActuatorsActuatorName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.updatedOn = [aDecoder decodeObjectForKey:kSMA_ActuatorsUpdatedOn];
    self.actuatorId = [aDecoder decodeDoubleForKey:kSMA_ActuatorsActuatorId];
    self.deviceId = [aDecoder decodeDoubleForKey:kSMA_ActuatorsDeviceId];
    self.actualValue = [aDecoder decodeDoubleForKey:kSMA_ActuatorsActualValue];
    self.createdOn = [aDecoder decodeObjectForKey:kSMA_ActuatorsCreatedOn];
    self.isActive = [aDecoder decodeDoubleForKey:kSMA_ActuatorsIsActive];
    self.actuatorName = [aDecoder decodeObjectForKey:kSMA_ActuatorsActuatorName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_updatedOn forKey:kSMA_ActuatorsUpdatedOn];
    [aCoder encodeDouble:_actuatorId forKey:kSMA_ActuatorsActuatorId];
    [aCoder encodeDouble:_deviceId forKey:kSMA_ActuatorsDeviceId];
    [aCoder encodeDouble:_actualValue forKey:kSMA_ActuatorsActualValue];
    [aCoder encodeObject:_createdOn forKey:kSMA_ActuatorsCreatedOn];
    [aCoder encodeDouble:_isActive forKey:kSMA_ActuatorsIsActive];
    [aCoder encodeObject:_actuatorName forKey:kSMA_ActuatorsActuatorName];
}

- (id)copyWithZone:(NSZone *)zone
{
    SMA_Actuators *copy = [[SMA_Actuators alloc] init];
    
    if (copy) {

        copy.updatedOn = [self.updatedOn copyWithZone:zone];
        copy.actuatorId = self.actuatorId;
        copy.deviceId = self.deviceId;
        copy.actualValue = self.actualValue;
        copy.createdOn = [self.createdOn copyWithZone:zone];
        copy.isActive = self.isActive;
        copy.actuatorName = [self.actuatorName copyWithZone:zone];
    }
    
    return copy;
}


@end
