//
//  Device.h
//  SmilingHome
//
//  Created by Om's on 09/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import "AbstractModel.h"

@interface Device : AbstractModel

@property (nonatomic , assign) int deviceID;
@property (nonatomic , assign) int userID;
@property (nonatomic , strong) NSString *deviceName;
@property (nonatomic , strong) NSString *updatedOn;
@property (nonatomic , assign) BOOL isActive;



+(instancetype) DeviceFromJSON: (NSDictionary *) JSON;

+(void) GetDevicesForUser:( int ) userID withHandler: (callbackHandler) handler;



+(void) AddDeviceForUser:(int) userID withName:(NSString *)deviceName handler:(callbackHandler)handler;

-(void) updateWithHandler:(callbackHandler)handler;
-(void) getSensorsAndActuators: (callbackHandler) handler;

@end
