//
//  User.h
//  SmilingHome
//
//  Created by Om's on 09/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import "AbstractModel.h"

@interface User : AbstractModel

@property (unsafe_unretained , nonatomic) int userID;
@property (strong , nonatomic) NSString *nickName;
@property (strong , nonatomic) NSString *userEmail;
@property (strong , nonatomic) NSString *createdOn;
@property (unsafe_unretained , nonatomic) BOOL isActive;

+(User *) UserWithJSON:(NSDictionary *)json;

+(void)GetUsersWithHandler:(callbackHandler) handler;

+(void)LoginUserWith:(NSString *)userName password:(NSString *)password andHandler:(callbackHandler) handler;
@end
