//
//  Device.m
//  SmilingHome
//
//  Created by Om's on 09/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import "Device.h"


//devices?user=1&all_data=0
/*
 {
 "device_name" : "MyEdison",
 "device_id" : 2,
 "user_id" : 1,
 "is_active" : 1
 }
 */

@interface Device ()

@property (nonatomic , strong) NSString *deviceKey;

@end

@implementation Device

-(instancetype)initWithDictionary:(NSDictionary *)dict {

	self = [super initWithDictionary:jsonDict];

	if (self) {

		self.deviceID = [jsonDict ok_intForKey:@"device_id"];
		self.userID = [jsonDict ok_intForKey:@"user_id"];
		self.deviceName = [jsonDict ok_safeStringValueForKey:@"device_name"];
		self.isActive = [jsonDict ok_boolForKey:@"is_active"];
		self.deviceKey = [jsonDict ok_safeStringValueForKey:@"device_key"];
		self.updatedOn = [jsonDict ok_safeStringValueForKey:@"updated_on"];
	}

	return self;
}


+(instancetype) DeviceFromJSON:(NSDictionary *) JSON {

	return [[Device alloc] initWithJSONDict:JSON];

}
@end
