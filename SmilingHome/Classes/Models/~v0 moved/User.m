//
//  User.m
//  SmilingHome
//
//  Created by Om's on 09/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import "User.h"
#import "SmillingPariAPIClient.h"


@implementation User

-(instancetype)initWithJSONDict:(NSDictionary *)jsonDict {

	self = [super initWithJSONDict:jsonDict];
	if (self) {
		/*
		 created_on" = "2014-11-29 16:54:29";
		 "is_active" = 1;
		 "nick_name" = TheGhost;
		 "user_email" = "khedekar.omkar@gmail.com";
		 "user_id" = 1;
		 */

		self.userID = [jsonDict ok_intForKey:@"user_id"];
		self.nickName = [jsonDict ok_safeStringValueForKey:@"nick_name"];
		self.userEmail = [jsonDict ok_safeStringValueForKey:@"user_email"];
		self.createdOn = [jsonDict ok_safeStringValueForKey:@"created_on"];
		self.isActive = [jsonDict ok_boolForKey:@"is_active"];
	}

	return self;
}

+(User *) UserWithJSON:(NSDictionary *)json {
	return [[User alloc] initWithJSONDict:json];
}

+(void)GetUsersWithHandler:(callbackHandler) handler{

	SmillingPariAPIClient *shared = [SmillingPariAPIClient sharedClient];

	[shared GET:@"users" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {

		DebugLog(@"responseObject : %@", responseObject);
		NSMutableArray *users = nil;
		
		if ([[self class] ok_isValidJSON:responseObject]) {

			NSDictionary *json = (NSDictionary *)responseObject;
			NSInteger status = [[json valueForKey:@"status"] integerValue];

			if ( status == 200) {

				NSArray *usersArray = [json objectForKey:@"users"];
				users = [NSMutableArray arrayWithCapacity:usersArray.count];

				for (NSDictionary*usrDict in usersArray) {

					User *user = [User UserWithJSON:usrDict];
					[users addObject:user];

				}

			}

		}

		if (handler) {
			handler(users , nil);
		}

	} failure:^(NSURLSessionDataTask *task, NSError *error) {

		OK_ERROR(@"%@", error);
		
		if (handler) {
			handler(nil , error);
		}

	}];
}

+(void)LoginUserWith:(NSString *)userName password:(NSString *)password andHandler:(callbackHandler) handler {

	//user_email
	//user_pass

	SmillingPariAPIClient *shared = [SmillingPariAPIClient sharedClient];
	NSDictionary *params = @{@"user_email": userName , @"user_pass": password};

	[shared POST:@"/user/login" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (handler) {
			handler(responseObject , nil);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {

		OK_ERROR(@"%@", error);

		if (handler) {
			handler(nil , error);
		}

	}];

}

@end
