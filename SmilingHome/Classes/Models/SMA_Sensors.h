//
//  SMA_Sensors.h
//
//  Created by Omkar Khedekar on 12/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SMA_Sensors : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *sensorName;
@property (nonatomic, assign) double sensorId;
@property (nonatomic, strong) NSString *createdOn;
@property (nonatomic, assign) double isActive;
@property (nonatomic, strong) NSString *updatedOn;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
