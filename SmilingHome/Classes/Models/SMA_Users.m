//
//  SMA_Users.m
//
//  Created by Omkar Khedekar on 12/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SMA_Users.h"
#import "SmillingPariAPIClient.h"

NSString *const kSMA_UsersNickName = @"nick_name";
NSString *const kSMA_UsersUserEmail = @"user_email";
NSString *const kSMA_UsersCreatedOn = @"created_on";
NSString *const kSMA_UsersUserId = @"user_id";
NSString *const kSMA_UsersIsActive = @"is_active";


@interface SMA_Users ()


@end

@implementation SMA_Users


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super initWithDictionary:dict];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.nickName = [dict ok_safeStringValueForKey:kSMA_UsersNickName];
            self.userEmail = [dict ok_safeStringValueForKey:kSMA_UsersUserEmail];
            self.createdOn = [dict ok_safeStringValueForKey:kSMA_UsersCreatedOn];
            self.userId = [dict ok_intForKey:kSMA_UsersUserId];
            self.isActive = [dict ok_boolForKey:kSMA_UsersIsActive];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.nickName forKey:kSMA_UsersNickName];
    [mutableDict setValue:self.userEmail forKey:kSMA_UsersUserEmail];
    [mutableDict setValue:self.createdOn forKey:kSMA_UsersCreatedOn];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kSMA_UsersUserId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isActive] forKey:kSMA_UsersIsActive];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}



#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.nickName = [aDecoder decodeObjectForKey:kSMA_UsersNickName];
    self.userEmail = [aDecoder decodeObjectForKey:kSMA_UsersUserEmail];
    self.createdOn = [aDecoder decodeObjectForKey:kSMA_UsersCreatedOn];
    self.userId = [aDecoder decodeDoubleForKey:kSMA_UsersUserId];
    self.isActive = [aDecoder decodeDoubleForKey:kSMA_UsersIsActive];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_nickName forKey:kSMA_UsersNickName];
    [aCoder encodeObject:_userEmail forKey:kSMA_UsersUserEmail];
    [aCoder encodeObject:_createdOn forKey:kSMA_UsersCreatedOn];
    [aCoder encodeDouble:_userId forKey:kSMA_UsersUserId];
    [aCoder encodeDouble:_isActive forKey:kSMA_UsersIsActive];
}

- (id)copyWithZone:(NSZone *)zone
{
    SMA_Users *copy = [[SMA_Users alloc] init];
    
    if (copy) {

        copy.nickName = [self.nickName copyWithZone:zone];
        copy.userEmail = [self.userEmail copyWithZone:zone];
        copy.createdOn = [self.createdOn copyWithZone:zone];
        copy.userId = self.userId;
        copy.isActive = self.isActive;
    }
    
    return copy;
}

#pragma mark - WS 
+(void)GetUsersWithHandler:(callbackHandler) handler{

	SmillingPariAPIClient *shared = [SmillingPariAPIClient sharedClient];

	[shared GET:@"users" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {

		DebugLog(@"responseObject : %@", responseObject);
		NSMutableArray *users = nil;

		if ([[self class] ok_isValidJSON:responseObject]) {

			NSDictionary *json = (NSDictionary *)responseObject;
			NSInteger status = [[json valueForKey:@"status"] integerValue];

			if ( status == 200) {

				NSArray *usersArray = [json objectForKey:@"users"];
				users = [NSMutableArray arrayWithCapacity:usersArray.count];

				for (NSDictionary*usrDict in usersArray) {

					SMA_Users *user = [SMA_Users modelObjectWithDictionary:usrDict];
					[users addObject:user];

				}

			}

		}

		if (handler) {
			handler(users , nil);
		}

	} failure:^(NSURLSessionDataTask *task, NSError *error) {

		OK_ERROR(@"%@", error);

		if (handler) {
			handler(nil , error);
		}

	}];
}

+(void)LoginUserWith:(NSString *)userName password:(NSString *)password andHandler:(callbackHandler) handler {

	//user_email
	//user_pass

	SmillingPariAPIClient *shared = [SmillingPariAPIClient sharedClient];
	NSDictionary *params = @{@"user_email": userName , @"user_pass": password};

	[shared POST:@"/user/login" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (handler) {
			handler(responseObject , nil);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {

		OK_ERROR(@"%@", error);

		if (handler) {
			handler(nil , error);
		}
		
	}];
	
}



@end
