//
//  SMA_Details.h
//
//  Created by Omkar Khedekar on 12/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SMA_Details : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *sensors;
@property (nonatomic, strong) NSArray *actuators;
@property (nonatomic, assign) double deviceId;
@property (nonatomic, assign) double isActive;
@property (nonatomic, strong) NSString *deviceName;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
