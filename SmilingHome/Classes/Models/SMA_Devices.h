//
//  SMA_Devices.h
//
//  Created by Omkar Khedekar on 12/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "AbstractModel.h"



@interface SMA_Devices : AbstractModel <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *updatedOn;
@property (nonatomic, strong) NSArray *actuators;
@property (nonatomic, assign) int deviceId;
@property (nonatomic, strong) NSString *deviceName;
@property (nonatomic, assign) int userId;
@property (nonatomic, assign) BOOL isActive;
@property (nonatomic, strong) NSArray *sensors;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
+(void)AddDeviceForUser:(int) userID withName:(NSString *)deviceName handler:(callbackHandler)handler;
+(void) GetDevicesForUser:( int ) userID withHandler:(callbackHandler) handler ;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

- (NSDictionary *)dictionaryRepresentation;

-(void) getSensorsAndActuators: (callbackHandler) handler;
-(void)updateWithHandler:(callbackHandler)handler;


@end
