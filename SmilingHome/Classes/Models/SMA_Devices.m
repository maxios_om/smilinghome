//
//  SMA_Devices.m
//
//  Created by Omkar Khedekar on 12/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "SMA_Devices.h"
#import "SMA_Actuators.h"
#import "SMA_Sensors.h"
#import "SmillingPariAPIClient.h"

NSString *const kSMA_DevicesUpdatedOn = @"updated_on";
NSString *const kSMA_DevicesActuators = @"actuators";
NSString *const kSMA_DevicesDeviceKey = @"device_key";
NSString *const kSMA_DevicesDeviceId = @"device_id";
NSString *const kSMA_DevicesDeviceName = @"device_name";
NSString *const kSMA_DevicesUserId = @"user_id";
NSString *const kSMA_DevicesIsActive = @"is_active";
NSString *const kSMA_DevicesSensors = @"sensors";


@interface SMA_Devices ()

@property (nonatomic, strong) NSString *deviceKey;

@end

@implementation SMA_Devices



+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super initWithDictionary:dict];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.updatedOn = [dict ok_safeStringValueForKey:kSMA_DevicesUpdatedOn];
    NSObject *receivedSMA_Actuators = [dict objectForKey:kSMA_DevicesActuators];
    NSMutableArray *parsedSMA_Actuators = [NSMutableArray array];
    if ([receivedSMA_Actuators isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedSMA_Actuators) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedSMA_Actuators addObject:[SMA_Actuators modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedSMA_Actuators isKindOfClass:[NSDictionary class]]) {
       [parsedSMA_Actuators addObject:[SMA_Actuators modelObjectWithDictionary:(NSDictionary *)receivedSMA_Actuators]];
    }

    self.actuators = [NSArray arrayWithArray:parsedSMA_Actuators];
            self.deviceKey = [dict ok_safeStringValueForKey:kSMA_DevicesDeviceKey ];
            self.deviceId = [dict ok_intForKey:kSMA_DevicesDeviceId ] ;
            self.deviceName = [dict ok_safeStringValueForKey:kSMA_DevicesDeviceName];
            self.userId = [dict ok_intForKey:kSMA_DevicesUserId];
            self.isActive = [dict ok_boolForKey:kSMA_DevicesIsActive];
    NSObject *receivedSMA_Sensors = [dict objectForKey:kSMA_DevicesSensors];
    NSMutableArray *parsedSMA_Sensors = [NSMutableArray array];
    if ([receivedSMA_Sensors isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedSMA_Sensors) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedSMA_Sensors addObject:[SMA_Sensors modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedSMA_Sensors isKindOfClass:[NSDictionary class]]) {
       [parsedSMA_Sensors addObject:[SMA_Sensors modelObjectWithDictionary:(NSDictionary *)receivedSMA_Sensors]];
    }

    self.sensors = [NSArray arrayWithArray:parsedSMA_Sensors];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.updatedOn forKey:kSMA_DevicesUpdatedOn];
    NSMutableArray *tempArrayForActuators = [NSMutableArray array];
    for (NSObject *subArrayObject in self.actuators) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForActuators addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForActuators addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForActuators] forKey:kSMA_DevicesActuators];
    [mutableDict setValue:self.deviceKey forKey:kSMA_DevicesDeviceKey];
    [mutableDict setValue:[NSNumber numberWithDouble:self.deviceId] forKey:kSMA_DevicesDeviceId];
    [mutableDict setValue:self.deviceName forKey:kSMA_DevicesDeviceName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kSMA_DevicesUserId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isActive] forKey:kSMA_DevicesIsActive];
    NSMutableArray *tempArrayForSensors = [NSMutableArray array];
    for (NSObject *subArrayObject in self.sensors) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForSensors addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForSensors addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForSensors] forKey:kSMA_DevicesSensors];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.updatedOn = [aDecoder decodeObjectForKey:kSMA_DevicesUpdatedOn];
    self.actuators = [aDecoder decodeObjectForKey:kSMA_DevicesActuators];
    self.deviceKey = [aDecoder decodeObjectForKey:kSMA_DevicesDeviceKey];
    self.deviceId = [aDecoder decodeDoubleForKey:kSMA_DevicesDeviceId];
    self.deviceName = [aDecoder decodeObjectForKey:kSMA_DevicesDeviceName];
    self.userId = [aDecoder decodeDoubleForKey:kSMA_DevicesUserId];
    self.isActive = [aDecoder decodeDoubleForKey:kSMA_DevicesIsActive];
    self.sensors = [aDecoder decodeObjectForKey:kSMA_DevicesSensors];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_updatedOn forKey:kSMA_DevicesUpdatedOn];
    [aCoder encodeObject:_actuators forKey:kSMA_DevicesActuators];
    [aCoder encodeObject:_deviceKey forKey:kSMA_DevicesDeviceKey];
    [aCoder encodeDouble:_deviceId forKey:kSMA_DevicesDeviceId];
    [aCoder encodeObject:_deviceName forKey:kSMA_DevicesDeviceName];
    [aCoder encodeDouble:_userId forKey:kSMA_DevicesUserId];
    [aCoder encodeDouble:_isActive forKey:kSMA_DevicesIsActive];
    [aCoder encodeObject:_sensors forKey:kSMA_DevicesSensors];
}

- (id)copyWithZone:(NSZone *)zone
{
    SMA_Devices *copy = [[SMA_Devices alloc] init];
    
    if (copy) {

        copy.updatedOn = [self.updatedOn copyWithZone:zone];
        copy.actuators = [self.actuators copyWithZone:zone];
        copy.deviceKey = [self.deviceKey copyWithZone:zone];
        copy.deviceId = self.deviceId;
        copy.deviceName = [self.deviceName copyWithZone:zone];
        copy.userId = self.userId;
        copy.isActive = self.isActive;
        copy.sensors = [self.sensors copyWithZone:zone];
    }
    
    return copy;
}


#pragma mark - WS 


+(void) GetDevicesForUser:( int ) userID withHandler:(callbackHandler) handler {

	SmillingPariAPIClient *shared = [SmillingPariAPIClient sharedClient];
	NSDictionary *params = @{
														@"user" : @(userID),
														@"all_data" : @(1)
													};
	[shared GET:@"devices" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {

		DebugLog(@"\nresponseObject : %@", responseObject);
		NSMutableArray *devices = nil;
		if ([[self class] ok_isValidJSON:responseObject]) {

			NSDictionary *json = (NSDictionary *)responseObject;
			NSInteger status = [[json valueForKey:@"status"] integerValue];

			if ( status == 200) {

				NSArray *usersArray = [json objectForKey:@"devices"];
				devices = [NSMutableArray arrayWithCapacity:usersArray.count];
				for (NSDictionary*usrDict in usersArray) {
					SMA_Devices *device = [SMA_Devices modelObjectWithDictionary:usrDict];
					[devices addObject:device];

				}

			}
		}

		if (handler) {
			handler(devices , nil);
		}

	} failure:^(NSURLSessionDataTask *task, NSError *error) {

		OK_ERROR(@"%@", error);

		if (handler) {

			handler(nil , error);

		}

	}];

}

+(void)AddDeviceForUser:(int) userID withName:(NSString *)deviceName handler:(callbackHandler)handler {

	NSDictionary *params = @{@"user_id": @(userID), @"device_name":deviceName};

	SmillingPariAPIClient *shared = [SmillingPariAPIClient sharedClient];
	[shared POST:@"/device/register" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (handler) {
			handler(responseObject , nil);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {

		OK_ERROR(@"%@", error);

		if (handler) {
			handler(nil , error);
		}

	}];
}

-(void)updateWithHandler:(callbackHandler)handler {

	NSDictionary *params = @{
							 @"device_id": @(self.deviceId),
							 @"device_name":self.deviceName,
							 @"is_active":@(self.isActive)
							 };

	SmillingPariAPIClient *shared = [SmillingPariAPIClient sharedClient];
	[shared POST:@"/device/update" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (handler) {
			handler(responseObject , nil);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {

		OK_ERROR(@"%@", error);
		if (handler) {
			handler(nil , error);
		}

	}];
}

-(void) getSensorsAndActuators: (callbackHandler) handler {
	NSDictionary *params = @{
							 @"device_id": @(self.deviceId)
							 };

	SmillingPariAPIClient *shared = [SmillingPariAPIClient sharedClient];
	[shared GET:@"/device" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (handler) {
			handler(responseObject , nil);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {

		OK_ERROR(@"%@", error);

		if (handler) {
			handler(nil , error);
		}
		
	}];
}



@end
