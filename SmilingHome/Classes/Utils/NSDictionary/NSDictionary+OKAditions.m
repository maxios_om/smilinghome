//
//  NSDictionary+OKAditions.m
//  AR_Cloud
//
//  Created by Om's on 06/12/12.
//
//

#import "NSDictionary+OKAditions.h"

@implementation NSDictionary (OKAditions)

-(NSInteger)ok_integerForKey:(id)key {
	id v = [self valueForKey:key];
	if (v) {
		return [v integerValue];
	}
	return 0;
}

-(int) ok_intForKey:(id)key {
	id v = [self valueForKey:key];
	if (v) {
		return [v intValue];
	}
	return 0;
}

-(BOOL) ok_boolForKey:(id)key {
	id v = [self valueForKey:key];
	if (v) {
		return [v boolValue];
	}
	return false;
}
-(float) ok_floatForKey:(id)key {

	id v = [self valueForKey:key];
	if (v) {
		return [v floatValue];
	}
	return 0.00;
}

-(double) ok_doubleForKey:(id)key {

	id v = [self valueForKey:key];
	if (v) {
		return [v doubleValue];
	}
	return 0.00;
}

-(NSString *) ok_safeStringValueForKey:(id)key {

	id tValue = [self valueForKey:key];
	if ([tValue isKindOfClass:[NSString class]] || [tValue isMemberOfClass:[NSString class]]) {

		return tValue;

	} else if(!tValue || tValue == [NSNull null]) {

		return @"";

	} else if([tValue isKindOfClass:[NSURL class]]) {
		return [(NSURL *) tValue absoluteString];
	} else if ([tValue respondsToSelector:@selector(stringValue)]){
		return [tValue stringValue];
	}
	return @"";
}


-(NSURL *) ok_urlForKey:(id)key {

	NSString *stringURL = [self  ok_safeStringValueForKey:key];

	NSMutableString *escapedString = [NSMutableString stringWithString:stringURL];

	[escapedString replaceOccurrencesOfString:@" " withString:@"+" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escapedString length])];

	return [NSURL URLWithString:escapedString];
}

-(BOOL) ok_havesValueForKey:(NSString *)key {
	BOOL result = YES;

	if ([[self valueForKey:key] isEqual:[NSNull null]] || [self valueForKey:key] == nil) {
		result = NO;
	}
	return result;
}

-(BOOL) writeToTextFile:(NSString *) filePath atomically:(BOOL)atomically{

	//create content - four lines of text
	//	NSArray *keys = [self allKeys];
	//	NSMutableString *content = [NSMutableString string];
	//	for (NSString*key in keys) {
	//
	//		[content appendString:key];
	//		[content appendFormat:@": %@ \n",[self valueForKey:key]];
	//	}


	//save content to the documents directory
	//	NSData *data = [NSData dataWithData:[content dataUsingEncoding:NSStringEncodingConversionAllowLossy]];
	NSString *content = [NSString stringWithFormat:@"%@" , self];
	NSError *error;
	BOOL status = [content writeToFile:filePath
							atomically:atomically
							  encoding:NSStringEncodingConversionAllowLossy
								 error:&error];

	NSLog(@"%@" , error);
	return status;
}

@end


@implementation NSMutableDictionary (OKAditions)
#pragma mark - Setters
-(void) ok_setBool:(BOOL)state forKey:(NSString *)defaultName {

	NSNumber *boolNumber = [NSNumber numberWithBool:state];
	[self setValue:boolNumber forKey:defaultName];

}
-(void) ok_setInteger:(NSInteger)state forKey:(NSString *)defaultName{

	NSNumber *boolNumber = [NSNumber numberWithInteger:state];
	[self setValue:boolNumber forKey:defaultName];

}

-(void) ok_setInt:(int)intValue forKey:(NSString *)defaultName {
	NSNumber *boolNumber = [NSNumber numberWithInt:intValue];
	[self setValue:boolNumber forKey:defaultName];
}
-(void) ok_setFloat:(float)state forKey:(NSString *)defaultName{

	NSNumber *boolNumber = [NSNumber numberWithFloat:state];
	[self setValue:boolNumber forKey:defaultName];
	
}
@end
