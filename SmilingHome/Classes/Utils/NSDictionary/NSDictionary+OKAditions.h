//
//  NSDictionary+OKAditions.h
//  AR_Cloud
//
//  Created by Om's on 06/12/12.
//
//

#import <Foundation/Foundation.h>

@interface NSDictionary (OKAditions)
/**
 *	Utility to check given key is valid or not 
 *	and do contain valid value for it or not
 *
 *	@param	key	Desired key as NSSTring
 *
 *	@return	Returns YES if given key haves non null value  
 */
- (BOOL) ok_havesValueForKey:(NSString *)key;

/**
 *	Utility methodes for getting targated Value from NSDictionery
 *
 */
-(NSInteger) ok_integerForKey:(id)key;
- (int) ok_intForKey:(id)key;
- (BOOL) ok_boolForKey:(id)key;
- (float) ok_floatForKey:(id)key;
- (double) ok_doubleForKey:(id)key ;
- (NSString *) ok_safeStringValueForKey:(id)key;
- (NSURL *) ok_urlForKey:(id)key;

@end

@interface NSMutableDictionary (OKAditions)
/**
 *	Utility methodes for setting targated Value to NSMutableDictionary
 *
 */
- (void) ok_setBool:(BOOL)boolValue forKey:(NSString *)defaultName;
- (void) ok_setInteger:(NSInteger)intValue forKey:(NSString *)defaultName;
- (void) ok_setInt:(int)intValue forKey:(NSString *)defaultName;
- (void) ok_setFloat:(float)floatValue forKey:(NSString *)defaultName;
@end
