//
//  ARCUtil.h
//
//  Created by Om's on 06/07/13.
//
//
#ifndef ARCUtil_h
#define ARCUtil_h

/**************** Support both ARC and non-ARC ********************/

#define OK_MY_CLASS(__ME__) [__ME__ class]
#define OK_WEEK_OBJECT(__OBJECT__) __WEAK typeof(__OBJECT__) 

#define IS_ARC_ENABLED (__has_feature(objc_arc_weak) || __has_feature(objc_arc))

#ifndef SUPPORT_ARC
#define SUPPORT_ARC

#if __has_feature(objc_arc_weak)                //objc_arc_weak
#define WEAK weak
#define __WEAK __weak
#define STRONG strong
#define AUTORELEASE self
#define RELEASE self
#define RETAIN self
#define CFTYPECAST(exp) (__bridge exp)
#define TYPECAST(exp) (__bridge_transfer exp)
#define CFRELEASE(exp) CFRelease(exp)
#define DEALLOC self

#elif __has_feature(objc_arc)                   //objc_arc
#define WEAK unsafe_unretained
#define __WEAK __unsafe_unretained
#define STRONG strong

#define AUTORELEASE self
#define RELEASE self
#define RETAIN self
#define CFTYPECAST(exp) (__bridge exp)
#define TYPECAST(exp) (__bridge_transfer exp)
#define CFRELEASE(exp) CFRelease(exp)
#define DEALLOC self

#else                                           //none
#define WEAK assign
#define __WEAK
#define STRONG retain

#define AUTORELEASE autorelease
#define RELEASE release
#define RETAIN retain
#define CFTYPECAST(exp) (exp)
#define TYPECAST(exp) (exp)
#define CFRELEASE(exp) CFRelease(exp)
#define DEALLOC dealloc

#endif
#endif

#define fooBar(exp) (exp)

/// BOXING
#define FBOX(x) [NSNumber numberWithFloat:x]
#define IBOX(x) [NSNumber numberWithInt:x]

#define OK_NONATONIC_STRONG @property (nonatomic , STRONG)
#define OK_NONATONIC_WEAK @property (nonatomic , WEAK)
#define OK_NONATONIC_ASSIGN @property (nonatomic , assign)

#if !defined(OK_EXTERN)
#  if defined(__cplusplus)
#   define OK_EXTERN extern "C"
#  else
#   define OK_EXTERN extern
#  endif
#endif

#if !defined(OK_INLINE)
# if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#  define OK_INLINE static inline
# elif defined(__cplusplus)
#  define OK_INLINE static inline
# elif defined(__GNUC__)
#  define OK_INLINE static __inline__
# else
#  define OK_INLINE static
# endif
#endif

#endif


