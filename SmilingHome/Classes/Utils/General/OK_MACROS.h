//
//  OK_MACROS.h
//  Proj_DoNotes
//
//  Created by Om's on 24/08/13.
//  Copyright (c) 2013 antom. All rights reserved.
//

#ifndef _OK_MACROS_h
#define _OK_MACROS_h


#define GENERATE_PRAGMA(x) _Pragma(#x)

/* Usage: Use TODO("some message") / FIXME("some message") / NOTE("some message") to generate appropriate warnings */
#define TODO(x) GENERATE_PRAGMA(message("[TODO] " x))
#define FIXME(x) GENERATE_PRAGMA(message("[FIXME] " x))
#define NOTE(x) GENERATE_PRAGMA(message("[NOTE] " x))
/*

#define NOT_IMPLEMENTED(warningMessage) \
[[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__] \
message:[NSString stringWithFormat:@"%s",warningMessage] \
delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]  show]; \
TODO("Implement this - " warningMessage)

#define SHOW_ERROR(_ERROR_) \
[[[UIAlertView alloc] initWithTitle:@"Error"] \
message:_ERROR_ \
delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]  show]; \
*/
#ifndef DebugLog 
#pragma mark -
#pragma mark Logging


#ifdef DEBUG
#define DebugLog(fmt, ...) NSLog(@"==\nFile: %s\nLine No: %d\nFunc: (%s)\n " fmt, __FILE__, __LINE__, __func__, ## __VA_ARGS__)
#define TRACE(fmt, ...) NSLog(@"%s:%d (%s): " fmt, __FILE__, __LINE__, __func__, ## __VA_ARGS__)
#else
// do nothing
#define DebugLog(fmt, ...)
#define TRACE(fmt, ...)
#endif

#define OK_ERROR(fmt, ...) NSLog(@"=====ERROR==== \n%s:%d (%s): " fmt, __FILE__, __LINE__, __func__, ## __VA_ARGS__)


#endif


#endif //_OK_MACROS_h
