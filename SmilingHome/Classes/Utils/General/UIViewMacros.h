//
//  UIViewMacros.h
//  AR_Cloud
//
//  Created by Om's on 01/02/13.
//
//

#ifndef AR_Cloud_UIViewMacros_h
#define AR_Cloud_UIViewMacros_h

#pragma mark - UIViewAutoresizing Macros


#define HEIGHT_OF_STATUS_BAR 20
#define HEIGHT_OF_TOOLBAR 44
#define HEIGHT_OF_TABLE_CELL 44
#define HEIGHT_OF_TAB_BAR 49
#define HEIGHT_OF_SEARCH_BAR 44
#define HEIGHT_OF_NAVIGATION_BAR 44
#define HEIGHT_OF_TEXTFIELD 31
#define HEIGHT_OF_PICKER 216
#define HEIGHT_OF_KEYBOARD 216
#define HEIGHT_OF_SEGMENTED_CONTROL 43
#define HEIGHT_OF_SEGMENTED_CONTROL_BAR 29
#define HEIGHT_OF_SEGMENTED_CONTROL_BEZELED 40
#define HEIGHT_OF_SWITCH 27
#define HEIGHT_OF_SLIDER 22
#define HEIGHT_OF_PROGRESS_BAR 9
#define HEIGHT_OF_PAGE_CONTROL 36



#define FLEX_BOTTOM		UIViewAutoresizingFlexibleBottomMargin
#define FLEX_TOP		UIViewAutoresizingFlexibleTopMargin
#define FLEX_LEFT		UIViewAutoresizingFlexibleLeftMargin
#define FLEX_RIGHT		UIViewAutoresizingFlexibleRightMargin
#define FLEX_WIDTH		UIViewAutoresizingFlexibleWidth
#define FLEX_HEIGHT		UIViewAutoresizingFlexibleHeight
#define FLEX_NONE		UIViewAutoresizingNone
#define FLEX_ALL		FLEX_BOTTOM | FLEX_TOP | FLEX_LEFT | FLEX_RIGHT | FLEX_WIDTH | FLEX_HEIGHT
#define FLEX_SIZE			FLEX_WIDTH | FLEX_HEIGHT
#define FLEX_ORIGIN			FLEX_BOTTOM | FLEX_TOP | FLEX_LEFT | FLEX_RIGHT
#define FLEX_START_PADDING		FLEX_TOP | FLEX_LEFT
#define FLEX_END_PADDING		FLEX_BOTTOM | FLEX_RIGHT

#define MAIN_SCREEN_BOUNDS [[UIScreen mainScreen] bounds]
#define APPLICATION_SCREEN_BOUNDS [[UIScreen mainScreen] bounds]

#define  IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define  IS_IPHONE_5 (MAIN_SCREEN_BOUNDS.size.height == 568)

#define  OK_DegreesToRadians (__ANGLE__) ((__ANGLE__) / 180.0 * M_PI)
#define  OK_RadiansToDegrees ( _radians_ )  ((_radians_) * 180/M_PI)


#ifdef __IPHONE_6_0
# define OK_TEXT_ALIGN_LEFT NSTextAlignmentLeft
# define OK_TEXT_ALIGN_CENTER NSTextAlignmentCenter
# define OK_TEXT_ALIGN_RIGHT NSTextAlignmentRight
#else
# define OK_TEXT_ALIGN_LEFT UITextAlignmentLeft
# define OK_TEXT_ALIGN_CENTER UITextAlignmentCenter
# define OK_TEXT_ALIGN_RIGHT UITextAlignmentRight
#endif


#ifdef __IPHONE_6_0
#define OK_LineBreak_WordWrapping NSLineBreakByWordWrapping
#define OK_LineBreak_CharWrapping NSLineBreakByCharWrapping
#define OK_LineBreak_Clipping  NSLineBreakByClipping
#define OK_LineBreak_TruncatingHead NSLineBreakByTruncatingHead
#define OK_LineBreak_TruncatingTail NSLineBreakByTruncatingTail
#define OK_LineBreak_TruncatingMiddle NSLineBreakByTruncatingMiddle

#else
#define OK_LineBreak_WordWrapping UILineBreakModeWordWrap
#define OK_LineBreak_CharWrapping UILineBreakModeCharacterWrap
#define OK_LineBreak_Clipping  UILineBreakModeClip
#define OK_LineBreak_TruncatingHead UILineBreakModeHeadTruncation
#define OK_LineBreak_TruncatingTail UILineBreakModeTailTruncation
#define OK_LineBreak_TruncatingMiddle UILineBreakModeMiddleTruncation

#endif


#define OK_UIStatusBarStyle UIStatusBarStyle

#ifdef __IPHONE_7_0
#define OK_UIStatusBarStyle_iOS_7 UIStatusBarStyleLightContent
#else
#define OK_UIStatusBarStyle_iOS_7 UIStatusBarStyleDefault 
#endif

#define OK_Current_Device_Version_Int  [[[UIDevice currentDevice] systemVersion]  intValue] 




#endif
