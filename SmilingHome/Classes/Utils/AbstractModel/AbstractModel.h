//
//  AbstractModel.h
//  AR_Cloud
//
//  Created by Om's on 11/02/13.
//
//

#import <Foundation/Foundation.h>
#import "NSDictionary+OKAditions.h"
#import "OK_MACROS.h"

@protocol AbstractModel

@optional
- (NSDictionary *)dictionaryRepresentation;

///TODO:
@required
//- (instancetype)initWithDictionary:(NSDictionary *)dict;
@end

/**
 *	simple call back handler block use as you require
 *
 */

typedef void (^callbackHandler)(id objectsOrObject , NSError *error );


/**
 *	Base class for every model class 
 *   
 */
@interface AbstractModel : NSObject <AbstractModel>

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;

/**
 *	Base Init methode so all derived classes will have same and default implemetation
 *
 *	@param	jsonDict Formatted JSON Dictionery
 *
 *	@return	new instance of class 
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict  __attribute((objc_requires_super));


@end

@interface AbstractModel (OK_JSONUtil)

+(BOOL)ok_isValidJSON:(id)object;

@end
