//
//  AbstractModel.m
//  AR_Cloud
//
//  Created by Om's on 11/02/13.
//
//

#import "AbstractModel.h"
#import "ARCUtil.h"


@implementation AbstractModel

+(instancetype)modelObjectWithDictionary:(NSDictionary *)dict {

	return [[AbstractModel alloc] init];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
	self = [super init];
	if (self) {
		
	}

	return self;
}

@end

@implementation AbstractModel (OK_JSONUtil)

+(BOOL)ok_isValidJSON:(id)object {

	BOOL isValid = NO;

	if ([object isKindOfClass:[NSDictionary class]]) {

		isValid = YES;
	}

	return isValid;
}

@end
