//
//  NSString+OK_Additions.m
//  Proj_HHSocialWrapper
//
//  Created by Om's on 02/08/13.
//  Copyright (c) 2013 HadHud. All rights reserved.
//

#import "NSString+OK_Additions.h"
#import "ARCUtil.h"
#import <UIKit/UIKit.h>

#define DECIMAL_NCR_PREFIX @"&#"
#define UNICODE_PREFIX @"\\U"

@implementation NSString (OK_Additions)

-(BOOL)ok_containsString:(NSString *)string compareOptions:(NSStringCompareOptions)options {
	NSRange rng = [self rangeOfString:string options:options];
	return rng.location != NSNotFound;
}

-(BOOL)ok_containsString:(NSString*)subString {

	return [self ok_containsString:subString compareOptions:1];
}


+(BOOL)ok_String:(NSString *)baseString contains:(NSString *)subString compareOptions:(NSStringCompareOptions)options {

	return [baseString ok_containsString:subString compareOptions:options];
}

+(BOOL)ok_String:(NSString *)baseString contains:(NSString *)subString {

	return [baseString ok_containsString:subString];
}




#pragma mark - DNCR

-(BOOL)ok_IsBlank {

	return [[self class] ok_StringIsBlank:self];
}

+(BOOL)ok_StringIsBlank:(NSString *)string {

	if (!string) {
		return YES;
	}
	if([[self ok_stringByStrippingWhitespace:string] length] == 0 || string == nil ) {
		return YES;
	}
	return NO;
}

+(NSString *)ok_stringByStrippingWhitespace :(NSString *)string {
	return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+(NSString *) ok_stringByStrippingHTML:(NSString *)s {
    NSRange r;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound) {
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    }
    return s;
}

+ (NSString *)ok_escapeSingleQuotes:(NSString *) stringParameter {

    NSString *theStr = [NSString stringWithString:stringParameter];

    NSString *escapedStr = [theStr stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
    
    return escapedStr;
}

#pragma mark - Convert NSString to Decimal NCR string

- (BOOL) ok_isDecimalNCRString
{
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF beginswith %@", DECIMAL_NCR_PREFIX];

	BOOL result = [test evaluateWithObject:self];
    return result;
}

- (BOOL) ok_isUnicodeString {

    return [self ok_containsString:UNICODE_PREFIX];
}

-(NSString*)ok_UTF8EncodedString {

	NSStringEncoding usedEncoding = 0;
	if ([self canBeConvertedToEncoding:NSASCIIStringEncoding]) {
		usedEncoding = NSASCIIStringEncoding;
	}

	if ([self canBeConvertedToEncoding:NSISOLatin1StringEncoding]) {

		usedEncoding = NSISOLatin1StringEncoding;
	}
	if (usedEncoding > 0) {

		NSData *data = [self dataUsingEncoding:usedEncoding];
		NSString *toString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		return toString;
	}


	return self;
}

-(NSString *) ok_UTF8DecodedString {

	NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
	NSString *toString = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];

	return toString;

}

+ (NSString *)ok_stringFromDecimalNCRString:(NSString *)decimalNCRString
{
    if (![decimalNCRString ok_isDecimalNCRString]) {
        return decimalNCRString;
    }

    NSArray *wordsInString = [decimalNCRString componentsSeparatedByString:@" "];

	int indexPointer = 0;
	NSString *returnString = @"";
    while(indexPointer != [wordsInString count]) {

        //	indexPointer --;

		NSString *WordStr = [wordsInString objectAtIndex:indexPointer];
		NSString *wordComponants = [WordStr stringByReplacingOccurrencesOfString:@"&#" withString:@""];
		NSArray *chunks = [wordComponants componentsSeparatedByString: @";"];
		NSString *unicodeForm= @"";

        for (int i=0; i<[chunks count]; i++) {

            int code = [[chunks objectAtIndex:i] intValue];

            if(code!=0) {

                unichar c = code;
                //	NSLog(@"C %c", c);
                unicodeForm = [unicodeForm stringByAppendingFormat:@"%C",c];
            }
            //NSLog(@"Word Received %@", arabicForm);

        }

        returnString = [returnString stringByAppendingFormat:@" %@", unicodeForm];
        indexPointer ++;

    }

	return returnString ;
}

+ (NSString *)ok_DecimalNCRFromString:(NSString *) str
{
    if ([str ok_isDecimalNCRString]) {
        return str;
    }

    int length = (int)str.length;

    NSMutableString *ncrEncodedStr = [[NSMutableString alloc] initWithCapacity:10];

    for(int i = 0; i < length; i++)  {
        unichar character = [str characterAtIndex:i];
        [ncrEncodedStr appendFormat:@"&#%d;", character];
    }

    return ncrEncodedStr;

}

- (NSString *) ok_Unicode
{
    NSMutableString *uniString = [ [ NSMutableString alloc ] init ];
    UniChar *uniBuffer = (UniChar *) malloc ( sizeof(UniChar) * [ self length ] );
    CFRange stringRange = CFRangeMake ( 0, [ self length ] );

    CFStringGetCharacters ( (CFStringRef)self, stringRange, uniBuffer );

    for ( int i = 0; i < [ self length ]; i++ ) {
        if ( uniBuffer[i] > 0x7e ) {
			UniChar uChar = uniBuffer[i];
			NSString *utf8String = [NSString stringWithFormat:@"\\u%04x", uChar];
			utf8String = [NSString stringWithCString:[utf8String cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSASCIIStringEncoding];
            [ uniString appendString:utf8String];
		}
        else {
            [ uniString appendFormat: @"%c", uniBuffer[i] ];
		}
    }

    free ( uniBuffer );

    NSString *retString = [ NSString stringWithString: uniString ];
	//    [ uniString release ];
	
    return retString;
}

/*
-(CGSize)getSizeForTextWithFont:(UIFont *)font andLineBreakMode:(OK_LineBreakMode)linebreakMode {

	CGSize maximumSize = CGRectInset([UIScreen mainScreen].bounds, 20, 20).size;

	CGSize expectedSize = [self sizeWithFont:font
								  constrainedToSize:maximumSize
									  lineBreakMode:linebreakMode];

	return expectedSize;
}
*/
@end
