//
//  NSString+OK_Additions.h
//  Proj_HHSocialWrapper
//
//  Created by Om's on 02/08/13.
//  Copyright (c) 2013 HadHud. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 Some handy additions on NSString
 @discussion
 provides usefull functions which helps in day today development 
 */
@interface NSString (OK_Additions)

-(BOOL)ok_containsString:(NSString *)string compareOptions:(NSStringCompareOptions)options ;

+(BOOL)ok_String:(NSString *)baseString contains:(NSString *)subString compareOptions:(NSStringCompareOptions)options ;

/**
 *	Utility method to check base string contains given substring or not
 *
 *	@param	subString	sub string to check
 *
 *	@return	'YES' if given string is present in base string 'NO' other wise
 */
- (BOOL)ok_containsString:(NSString*)subString;


/**
 *	Static Utility method to check base string contains given substring or not
 *  @see -containsString:
 *	@param	baseString	baseString 
 *	@param	subString	subString to check
 *	
 *	@return	'YES' if given string is present in base string 'NO' other wise
 */
+ (BOOL)ok_String:(NSString *)baseString contains:(NSString *)subString;

- (BOOL)ok_IsBlank;
+ (BOOL)ok_StringIsBlank:(NSString *)string ;

+ (NSString *)ok_stringByStrippingWhitespace :(NSString *)string ;
+ (NSString *)ok_escapeSingleQuotes:(NSString *) stringParameter;
+ (NSString *)ok_stringByStrippingHTML:(NSString *)s;


- (BOOL) ok_isDecimalNCRString;
- (BOOL) ok_isUnicodeString ;
- (NSString*)ok_UTF8EncodedString;
- (NSString *)ok_UTF8DecodedString;

+ (NSString *)ok_stringFromDecimalNCRString:(NSString *)decimalNCRString;
+ (NSString *)ok_DecimalNCRFromString:(NSString *) str;
/*
#ifdef __IPHONE_7_0

#define OK_LineBreakMode NSLineBreakMode

#else

#define OK_LineBreakMode UILineBreakMode

#endif

- (CGSize)getSizeForTextWithFont:(UIFont *)font andLineBreakMode:(OK_LineBreakMode)linebreakMode;
*/
@end
