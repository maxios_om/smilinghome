//
//  NSString+OKTokenizer.m
//  ProjWordGame
//
//  Created by Om's on 25/07/13.
//  Copyright (c) 2013 HadHud. All rights reserved.
//

#import "NSString+OKTokenizer.h"

@interface HTMLCharMap : NSObject
@property (nonatomic , strong) NSString *symbol;
@property (nonatomic , strong) NSString *HTML_Number;
@property (nonatomic , assign) int ASCII_Dec;
+(HTMLCharMap *)mapForCharacter:(NSString *)stringChar;
@end

@implementation HTMLCharMap

+(NSArray *)getAsciiHTMLCharMap {

	static dispatch_once_t onceToken;
	static NSMutableArray *AsciiHTMLCharMap ;
	dispatch_once(&onceToken, ^{

		AsciiHTMLCharMap = [NSMutableArray array];

		for (int start = 31; start <= 47 ; start ++) {

			HTMLCharMap *charMap = [[HTMLCharMap alloc] init];
			charMap.ASCII_Dec = start;
			charMap.symbol = [NSString stringWithFormat:@"%c", start];
			charMap.HTML_Number = [NSString stringWithFormat:@"&#%d",start];

		}

	});

	return AsciiHTMLCharMap;
}

+(HTMLCharMap *)mapForCharacter:(NSString *)stringChar {
	NSArray *ascciiMap = [HTMLCharMap getAsciiHTMLCharMap];

	for (HTMLCharMap*map in ascciiMap) {

		if ([map.symbol isEqualToString:stringChar]) {

			return map;
		}
	}

	return nil;
}

@end

@implementation NSString (OKTokenizer)

- (NSString *)getFormatedString
{
    return [[[[NSString stringWithString:self] componentsSeparatedByCharactersInSet:[[NSCharacterSet letterCharacterSet] invertedSet]] componentsJoinedByString:@""] lowercaseString];
}

///!!!: Convert String to Array Of Characters

-(NSMutableArray *)getCharArray {

	NSMutableArray *charArray = [NSMutableArray array];

	NSString *formattedString = [self getFormatedString];


	for (int i = 0; i < formattedString.length; i++) {

		NSString * newString = [formattedString substringWithRange:NSMakeRange(i, 1)];
		if (![newString isEqualToString:@""]) {

			[charArray addObject:newString];

		}
	}

	return charArray;
}

- (BOOL)containsString:(NSString *)string
               options:(NSStringCompareOptions)options {
	NSRange rng = [self rangeOfString:string options:options];
	return rng.location != NSNotFound;
}

- (BOOL)containsString:(NSString *)string {
	return [self containsString:string options:0];
}

+(NSString *)stringFromCharacters:(NSMutableArray *)chars {

	NSMutableString *newString = [NSMutableString string];
	for (NSString*ch in chars) {

		[newString appendString:ch];
	}

	return newString;
}

+ (NSString*)randomStringChar {

	int rangeLow = 65;
	int rangeHigh = 90;
	int randomNumber = arc4random() % (rangeHigh-rangeLow+1) + rangeLow;

	return [NSString stringWithFormat:@"%c", randomNumber];
}



+(NSString *)ok_htmlFormatedStringFromString:(NSString *)string {

	
	NSMutableString *newMutableString = [NSMutableString stringWithString:string];
	
	for (int i = 0; i < string.length; i++) {
		NSRange range = NSMakeRange(i, 1);
		NSString * newString = [string substringWithRange:range];
		HTMLCharMap *ascciiMap = [HTMLCharMap mapForCharacter:newString];
		if (ascciiMap) {

			[newMutableString replaceCharactersInRange:range withString:ascciiMap.HTML_Number];

		}
	}

	return newMutableString;
}

-(NSString *)ok_fileNameForResource {
	NSString *truncated = [self lastPathComponent];
	return [truncated stringByDeletingPathExtension];
}
-(NSString *)ok_extenctionForResource {
	NSString *truncated = [self lastPathComponent];
	return [truncated pathExtension];
}

@end
