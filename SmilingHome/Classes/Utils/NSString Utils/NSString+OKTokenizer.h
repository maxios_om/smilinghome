//
//  NSString+OKTokenizer.h
//  ProjWordGame
//
//  Created by Om's on 25/07/13.
//  Copyright (c) 2013 HadHud. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (OKTokenizer)

- (NSString *) getFormatedString;
- (NSMutableArray *) getCharArray ;
- (BOOL)containsString:(NSString *)string;
- (BOOL)containsString:(NSString *)string
               options:(NSStringCompareOptions)options;

+ (NSString*) randomStringChar;
+ (NSString *) stringFromCharacters:(NSMutableArray *)chars;

+(NSString *)ok_htmlFormatedStringFromString:(NSString *)string;

-(NSString *)ok_fileNameForResource;
-(NSString *)ok_extenctionForResource;

@end
