//
//  ViewController.m
//  SmilingHome
//
//  Created by Om's on 09/12/14.
//  Copyright (c) 2014 Oms. All rights reserved.
//

#import "ViewController.h"
#import "SMA_Users.h"
#import "UserSessionManager.h"
#import "UserDevicesTableViewController.h"

static NSString *cellIDF = @"TABLEVIEWCELL_USER";

@interface ViewController ()

@property (strong , nonatomic) NSMutableArray *users;

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];

	[self setTitle:@"Users"];
/*
	[SMA_Users GetUsersWithHandler:^(NSMutableArray *users, NSError *error) {
		if (users) {
			self.users = users;
			[self.tableView reloadData];
		} else {
			if (error) {
				///TODO:
			}
		}
	}];
*/

	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate & datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

	return self.users.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {


	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIDF];

	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIDF];
	}

	SMA_Users *user = self.users [indexPath.row];
	if (user) {
		cell.textLabel.text = user.nickName;
		cell.detailTextLabel.text = user.createdOn;
	}
	return cell;
}



#pragma mark - UIStoryboardSegue


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"ShowDevices"])
	{
//		UserDevicesTableViewController *userDevicesController = [segue destinationViewController];

		NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
		[[UserSessionManager sessionManager] setCurrentUser:self.users[selectedIndexPath.row]];


	}
}


@end
