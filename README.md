
## Smiling Home (home automation and stats)
> Smiling Home. 

This is iOS front end of POC done for demonstrating thermal stats gathering and basic intraction with controlls like light. 
Represents third layer i.e. Application

Currently supports 
- [x] Thermo stats
- [x] Intraction with connected Actuators

## Terms
    - Sensors: Devices which gather useful information like thermo stats 
    - Actuators: Devices that can control a mechanism or system like room light

## In field setup
    - Raspberri PI connected with Internet acting as in field gateway.
    - Thermal sensors with ESP32 conncted with gateway.
    - Light (LED) control units with ESP32 with gateway.

## Requirements

- iOS 9.0+
- Xcode 8+

## Running 
open using cmd line `xed . `
or simply open `SmilingHome.xcworkspace` 
`cmd+r` and we are running. 


## Notes:
This is an old project done for very basic demonstration purpose long time back so some parts are not active at this time.

## Credits
- [AFNetworking](https://github.com/AFNetworking/AFNetworking)
- [MBProgressHUD](https://github.com/jdg/MBProgressHUD)

## Meta

Omkar – [@MAD_Omkar](https://twitter.com/MAD_OmkAR)

[ARomkAR](https://github.com/ARomkAR)